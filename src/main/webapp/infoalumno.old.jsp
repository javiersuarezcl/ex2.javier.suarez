<%-- 
    Document   : infoalumno
    Created on : 01-06-2021, 10:57:33
    Author     : JSuarez
--%>

<%@page import="cl.ex2javiersuarez.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Alumnos alumno = (Alumnos) request.getAttribute("alumno");

%>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <title>Información de alumno</title>
    </head>
    <body bgcolor="#87CEFA" text="#3399ff">
    <center>
        <form name="form" id="update" action="NOController" method="POST">
            <h1>Se ha encontrado el siguiente alumno:</h1>
            <h2>Modificalo!</h2>
            RUT: <input name="rut" value="<%= alumno.getRut()%>">
            <br>Nombre (s): <input name="nombre" value="<%= alumno.getNombre()%>">
            <br>Apellidos: <input name="apellidos" value="<%= alumno.getApellidos()%>">
            <br>Fecha de nacimiento: <input name="fnac" value="<%= alumno.getFnac()%>">
            <br>Carrera: <input name="carrera" value="<%= alumno.getCarrera()%>">

            <br><br><button class="boton" type="submit" value="Modificar">Modificar</button>
            <br><br><input type="button" value="Volver" onclick="location.href = 'index.jsp';" />
        </form>
    </center>
</body>
</html>

