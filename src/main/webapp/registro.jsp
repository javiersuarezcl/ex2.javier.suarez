<%-- 
    Document   : registro
    Created on : 30-05-2021, 21:54:55
    Author     : JSuarez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <title>Registro</title>
    </head>
    <body bgcolor="#87CEFA" text="#3399ff">
    <center>
        <h1>Ingreso de alumnos</h1>
        <h2>Anote sus datos de alumno:</h2><br>

        <form  name="form" action="CreateController" method="POST">
            <div class="form-group">
                Rut
                <input name="rut" value="" required id="rut">
            </div>
            <br>
            <div class="form-group">
                Nombres
                <input name="nombre" value="" required id="nombre">
            </div>       
            <br>
            <div class="form-group">
                Apellidos
                <input name="apellidos" value="" required id="apellidos">
            </div> 
            <br>
            <div class="form-group">
                Fecha de nacimiento
                <input type="date" name="fnac" value="" required id="fnac">
            </div> 
            <br>
            <div class="form-group">
                Carrera
                <input name="carrera" value="" required id="carrera">
            </div> 
            <br><br>
            <button type="submit" name="alumno" value="ingresar" class="btn btn-success">Registrar</button>
            <input type="reset" value="Limpiar">
            <br><br><input type="button" value="Volver" onclick="location.href='index.jsp';" />
        </form>
    </body>
</html>
