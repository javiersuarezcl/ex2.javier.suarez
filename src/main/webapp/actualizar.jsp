<%-- 
    Document   : actualizar
    Created on : 02-06-2021, 21:46:43
    Author     : JSuarez
--%>

<%@page import="cl.ex2javiersuarez.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Alumnos alumno = (Alumnos) request.getAttribute("alumno");
%>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <title>Actualización de alumno</title>
    </head>
    <body bgcolor="#87CEFA" text="#3399ff">
    <center>
        <h1>Modificación de alumno</h1>
        <h2>Anote un rut para encontrar al alumno:</h2><br>               

        <form name="form" id="read" action="ReadController" method="POST">
            Rut de alumno:
            <input type="text" name="Rut" id="Rut">  
            <br><br><br>
            <input type="submit" value="Consultar">
            <input type="reset" value="Limpiar">
            <br><br><input type="button" value="Volver" onclick="location.href = 'index.jsp';" />
        </form>

    </center>
</body>
</html>
