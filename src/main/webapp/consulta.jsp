<%-- 
    Document   : consulta
    Created on : 01-06-2021, 10:26:35
    Author     : JSuarez
--%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.ex2javiersuarez.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /*Alumnos alumno = (Alumnos) request.getAttribute("alumno");*/
    List<Alumnos> lista = (List<Alumnos>) request.getAttribute("listaAlumnos");
    Iterator<Alumnos> itAlumnos = lista.iterator();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <title>Consulta</title>
    </head>
    <body bgcolor="#87CEFA" text="#3399ff">
    <center>
        <h1>Ingreso de alumnos</h1>
        <h2>Lista de alumnos y/o compañeros:</h2><br>            
        
            <% if ((lista != null)) { %>

            <table border="1">
                <thead>
                <th>RUT</th>
                <th>Nombre(s)</th>
                <th>Apellido(s)</th>
                <th>Fecha de nacimiento</th>
                <th>Carrera</th>
                </thead>
                <tbody>
                    <%while (itAlumnos.hasNext()) {
                            Alumnos alumno = itAlumnos.next();%>
                    <tr>
                        <td><%=alumno.getRut()%></td>
                        <td><%=alumno.getNombre()%></td>
                        <td><%=alumno.getApellidos()%></td>
                        <td><%=alumno.getFnac()%></td>
                        <td><%=alumno.getCarrera()%></td>
                    </tr>
                    <%}%>
                </tbody>           
            </table>
            <br><br><input type="button" value="Volver" onclick="location.href = 'index.jsp';" />

            <%}%>      






    </center>
</body>
</html>
