package cl.ex2javiersuarez.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.ex2javiersuarez.dao.AlumnosJpaController;
import cl.ex2javiersuarez.entity.Alumnos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author JSuarez
 */
@WebServlet(name = "ReadController", urlPatterns = {"/ReadController"})
public class ReadController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReadController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReadController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AlumnosJpaController dao = new AlumnosJpaController();
        List<Alumnos> listaAlumnos = dao.findAlumnosEntities();
        request.setAttribute("listaAlumnos", listaAlumnos);

        request.getRequestDispatcher("consulta.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
             String rut = request.getParameter("Rut");

        System.out.println("El usuario es : " + rut);

        AlumnosJpaController dao = new AlumnosJpaController();

        Alumnos alumno = dao.findAlumnos(rut);
        /*Alumnos alumno = dao.findAlumnos(nombre);
        Alumnos alumno = dao.findAlumnos(apellidos);
        Alumnos alumno = dao.findAlumnos(fnac);
        Alumnos alumno = dao.findAlumnos(carrera);*/

        if (alumno != null) {
            
            request.setAttribute("alumno", alumno);
                    
            request.getRequestDispatcher("actualizaralumno.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
